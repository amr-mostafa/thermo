TODO
####

1. Write tests
2. CI (e.g. ``.gitlab-ci.yml`` or so)
3. Make API return consistent values between a query and a subscription. We already solved
   that problem for simultaneous subscriptions, but consistency with query remains
4. Add a ``unit`` arg to the query and subscription to allow choosing between C/F and have
   the API do the conversion

Overview
########

This is Thermo, a service that provides an API for temperature measurements taken from supported
connected devices.


Requirements
############

1. Python >= 3.8
2. Docker (or redis installed locally)


Supported thermostats
#####################

* Random number generator

To add support for more thermostats, you need to create a class that extends
``temperature.thermostat.protocol.Protocol`` and register it with the thermostat manager, e.g.::

  # in temperature.thermostat.manager.py
  manager.register(YourNewThermostatProbe(), "my-new-thermostat")

Then when starting the ``temperature-reader`` service, you pass the name of the thermostat you would
like to read from::

  dotenv run ./manage.py read_thermostat --interval 1 my-new-thermostat

Or by modifying ``docker-compose.yml``::

  services:
    ...
    temperature-reader:
      ...
      command: python manage.py read_thermostat --interval 1 my-new-thermostat


The temperature reader actually supports reading multiple thermostats, so you may pass multiple
thermostat names. However, to make full use of that, we need to improve few other things in our
current implementation:

1. Django commands run synchronously by default, so even though our thermostats report temperatures
   in an async fashion, the temperature reader invokes them synchronously. It shouldn't be hard to
   create an asyncio event loop manually and have the temperature reader invoke all thermostats
   in an async fashion
2. The GraphQL query and subscription need to be extended to allow passing an argument of which
   thermostat they are interested in reading/subscribing-to, and perhaps our core
   ``TemperatureReading`` model could also be extended with a new field ``thermostat`` that stores
   the name of the thermostat from which this reading came from.

How to use
##########

Run the project locally
=======================

Run everything in Docker, using ``docker-compose.yml``::

  docker-compose up -d

Run Redis in Docker, but run the Python services locally::

  # Install poetry (https://python-poetry.org/docs/#installation)
  # Check next section if you don't want to install Poetry
  curl -sSL https://raw.githubusercontent.com/python-poetry/poetry/master/get-poetry.py | python -

  # Install project dependencies
  cd <project-root>
  poetry install

  # Optional (uncomment next 2 lines), make poetry create a venv locally in the project (under .venv)
  # poetry config virtualenvs.create true
  # poetry config virtualenvs.in-project true

  # Configure environment
  cp sample.env .env
  # vim .env and update it to match your environment

  # We need to run 3 things

  # 1: Redis
  # Omit -d if you'd rather start it in the foreground
  docker-compose up -d redis

  # 2: API
  # Step into the venv
  poetry shell
  dotenv run ./manage.py runserver

  # 3: Temperature reader / producer
  # In a new terminal tab
  cd <project-root>
  # Step into the venv on that tab
  poetry shell
  # Start the producer, and have it probe the "random" thermostat every "1" second
  dotenv run ./manage.py read_thermostat --interval 1 random

We include a ``requirements.txt`` (exported by ``poetry``) which can be used with ``pip``. But
you will have to adapt the above instructions yourself at the moment.

Access the GraphiQL UI
======================

Visit http://127.0.0.1:8000/graphql

Sample query & subscription for your convenience::

  subscription currentTemperatureSubscribe {
    currentTemperature {
      temperature {
        timestamp
        value
        unit
      }
    }
  }

  query currentTemperature {
    currentTemperature {
      timestamp
      value
      unit
    }
  }

Development
###########

Code style
==========

We follow PEP-8 and locally use `Black <https://github.com/psf/black>`_ for automatically formatting
code for PEP-8 compliance. In the future we might add a pre-commit hook to run black on committed
files and a validation step as part of CI. We completely understand that this sort of auto
formatting might be too aggressive for some, but hold on to your judgement until you have tried it.
The biggest feature of an auto formatter is the new mental model for code formatting: ``None``. One
less thing to worry about.

We also use python types as much as possible, supported by ``mypy``.


FAQ/NOTES
#########

1. What's ``pyproject.toml``?
=============================

It's an emerging Python PEP standard that is used for declaring project dependencies, and is gaining
momentum. In this project it's used primarily for declaring runtime and dev dependencies. We use
`Poetry <https://github.com/python-poetry/poetry>`_ for managing dependencies.

See `PEP-518 <https://www.python.org/dev/peps/pep-0518/>`_.

2. We use celsius
=================

Much like how we default to Unicode/UTF-8 for strings, we use celsius for temperatures
everywhere. We only convert to fahrenheit (if desired) at the edges of the API as we send/receive
output/input.

This means our code can simply pass a `float` to represent a temperature instead of having to pass
``Temperature(value=..., unit=...)`` and keep converting back and forth everywhere.
