import graphene

import temperature.schema


class Query(temperature.schema.Query, graphene.ObjectType):
    """Project level GraphQL root for queries"""

    pass


class Subscription(temperature.schema.Subscription, graphene.ObjectType):
    """Project level GraphQL root for subscriptions"""

    pass


schema = graphene.Schema(query=Query, subscription=Subscription)
