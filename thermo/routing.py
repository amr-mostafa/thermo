from django.urls import path
from channels.routing import ProtocolTypeRouter, URLRouter

from thermo.subscription import GraphqlWsConsumer

application = ProtocolTypeRouter(
    {
        "websocket": URLRouter(
            [
                path("graphql", GraphqlWsConsumer),
            ]
        ),
    }
)
