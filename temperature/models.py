from django.db import models


class TemperatureUnit(models.TextChoices):
    CELSIUS = "C"
    FAHRENHEIT = "F"


class TemperatureReading(models.Model):
    """Represents a temperature reading at a given point in time"""

    timestamp = models.DateTimeField()
    value = models.FloatField()
    unit = models.CharField(
        max_length=2, choices=TemperatureUnit.choices, default=TemperatureUnit.CELSIUS
    )

    def __str__(self):
        return (
            f"Temperature reading of '{self.value} {self.unit}' at '{self.timestamp}'"
        )
