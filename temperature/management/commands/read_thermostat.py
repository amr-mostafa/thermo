import datetime
import signal
from argparse import ArgumentParser
from time import sleep
from typing import List, Any, TypedDict, Optional

from asgiref.sync import async_to_sync
from django.core.management.base import BaseCommand, CommandError

from temperature.models import TemperatureReading
from temperature.schema import TemperatureUnit, CurrentTemperatureSubscription
from temperature.thermostat.manager import manager

# Django commands do not support async at the moment
read_temperature_sync = async_to_sync(manager.read_temperature)


class CommandOpts(TypedDict):
    thermostat_names: List[str]
    interval: int


class Command(BaseCommand):
    help = "Repeatedly read thermostats and publishes them to a channel"

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.stopped = False

    def add_arguments(self, parser: ArgumentParser) -> None:
        parser.add_argument(
            "thermostat_names", metavar="thermostat_name", nargs="+", type=str
        )
        parser.add_argument(
            "--interval", type=int, default=1, help="Read interval in seconds"
        )

    def stop(self, sig: Optional[int] = None, frame: Optional[Any] = None):
        self.stopped = True

    def handle(self, *args: Any, **options: Any) -> None:
        secs: int = options["interval"]

        signal.signal(signal.SIGINT, self.stop)
        signal.signal(signal.SIGTERM, self.stop)

        while not self.stopped:
            for name in options["thermostat_names"]:
                try:
                    temperature = read_temperature_sync(name)
                    CurrentTemperatureSubscription.publish_temperature_reading(
                        TemperatureReading(
                            timestamp=datetime.datetime.now(),
                            value=temperature,
                            unit=TemperatureUnit.CELSIUS,
                        )
                    )
                    self.stdout.write(
                        self.style.SUCCESS(
                            f"Successfully published temperature '{temperature}C' "
                            f"from thermostat '{name}'"
                        )
                    )
                except KeyError:
                    raise CommandError(f"Thermostat with name '{name}' does not exist")
            sleep(secs)

        print("\nReceived SIGINT or SIGTERM, exiting")
