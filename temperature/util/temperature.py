def c_to_f(temperature: float) -> float:
    return temperature * 9 / 5 + 32
