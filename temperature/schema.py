from __future__ import annotations
import datetime
from typing import List, Optional, Union

import channels_graphql_ws
import graphene
from graphene_django import DjangoObjectType

from temperature.models import TemperatureUnit, TemperatureReading
from temperature.thermostat.manager import manager


class GraphqlTemperatureReading(DjangoObjectType):
    class Meta:
        model = TemperatureReading


class Query(graphene.AbstractType):
    current_temperature = graphene.Field(
        GraphqlTemperatureReading, name="currentTemperature"
    )

    @staticmethod
    def resolve_current_temperature(root, info) -> GraphqlTemperatureReading:
        return GraphqlTemperatureReading(
            timestamp=datetime.datetime.now(),
            value=manager.read_temperature_sync("random"),
            unit=TemperatureUnit.CELSIUS,
        )


class CurrentTemperatureSubscription(channels_graphql_ws.Subscription):
    """GraphQL subscription for the current temperature reading"""

    # GraphQL fields returned from that subscription
    temperature = graphene.Field(GraphqlTemperatureReading)

    @staticmethod
    def subscribe(root, info) -> Optional[List[str]]:
        """Called when a client subscribes. Return a list of channels group names or None"""
        return None

    @staticmethod
    def publish(
        payload: TemperatureReading, info
    ) -> Union[CurrentTemperatureSubscription, CurrentTemperatureSubscription.SKIP]:
        """Called when we receive an event"""

        return CurrentTemperatureSubscription(
            temperature=GraphqlTemperatureReading(
                timestamp=payload.timestamp,
                value=payload.value,
                unit=payload.unit,
            )
        )

    @staticmethod
    def publish_temperature_reading(reading: TemperatureReading) -> None:
        CurrentTemperatureSubscription.broadcast(payload=reading)


class Subscription(graphene.AbstractType):
    """Root GraphQL field for subscriptions"""

    current_temperature = CurrentTemperatureSubscription.Field(
        name="currentTemperature"
    )
