from typing import Dict

from asgiref.sync import async_to_sync

from temperature.thermostat.protocol import Thermostat
from temperature.thermostat.random import RandomNumberThermostat


class ThermostatManager:
    """
    A registry for all managed thermostats, allows reading a thermostat by
    using their registered name
    """

    def __init__(self) -> None:
        self.thermostats: Dict[str, Thermostat] = {}

    def register(self, thermostat: Thermostat, name: str) -> None:
        self.thermostats[name] = thermostat

    async def read_temperature(self, name: str) -> float:
        if name not in self.thermostats:
            raise KeyError(f"Unknown thermostat: {name}")

        return await self.thermostats[name].read_temperature()

    read_temperature_sync = async_to_sync(read_temperature)


manager = ThermostatManager()
manager.register(RandomNumberThermostat(), "random")
