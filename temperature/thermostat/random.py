import random

from temperature.thermostat.protocol import Thermostat


class RandomNumberThermostat(Thermostat):
    async def read_temperature(self) -> float:
        return random.uniform(-50.0, 100.0)
