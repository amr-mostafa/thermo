from typing import Protocol


class Thermostat(Protocol):
    """Protocol definition for all thermostats"""

    async def read_temperature(self) -> float:
        """Read and return thermostat temperature in Celsius"""
